{
  "latest": "ORSC-5.4.3",
  "notes": [
    {
      "title": "ORSC-5.4.3",
      "date": "2020-07-19",
      "changes": [
        {
          "type": "Bug Fixes",
          "details": [
            "Spells can now hit a 0.",
            "Players can now use magic against all the shapeshifter forms while in combat with it during Witch's House.",
            "Shadow Warriors now drop more than just their bones.",
            "Rare and Ultra Rare Drop Table rates have been slightly tweaked.",
            "Blue wizards now drop the correct wizard hat",
            "Player moderators are now visible to normal players in the online list."
          ]
        },
        {
          "type": "Core Improvements",
          "details": [
            "Reorganized the plugin folder hierarchy.",
            "Rewrote the command infrastructure."
          ]
        }
      ]
    },
    {
      "title": "ORSC-5.4.2",
      "date": "2020-07-15",
      "changes": [
        {
          "type": "Bug Fixes",
          "details": [
            "Players can now swap weapons while in all types of combat",
            "Fixed a plugin crash caused by clicking away from Mum",
            "Fixed a crash with NPC retreats when the opponent has been removed",
            "Fixed a crash when clicking the Forgot Password button",
            "Fixed an issue where hobgoblins weren't dropping limpwurt roots",
            "Fixed a bug where the guard wouldn't catch people who were stealing cakes",
            "Replaced missing custom scenery/objects that were lost in the migration to JSON",
            "Quest NPCs that have death conditions (Count Draynor, Delrith, ect.) are now able to be killed properly",
            "Added a check for a null script context, fixes some plugins crashing.",
            "Fixed Gnome waiter menu options (trade/pickpocket)",
            "Removed King Lathas trade option",
            "Fixed a bug where the auction house couldn't be refreshed by some",
            "Fixed an issue where players couldn't swap weapons while in combat.",
            "Fixed an issue where you had to wait a long time in between attempting to cut the webs in the Wilderness",
            "Fixed an issue where stat draining NPCs would cause an infinite loop (specifically Kolodion and Salarin)"
          ]
        },
        {
          "type": "Enhancements",
          "details": [
            "Fishing spots now have a lower chance to turn into a pebble",
            "Teddy parts must be brought to the child before you can stitch them",
            "Can no longer get stuck behind lockpick doors if fatigued.",
            "Ensure all players show in the count when doing ::online regardless of privacy setting",
            "Removed some naughty words from the sleep words list"
          ]
        },
        {
          "type": "Core Improvements",
          "details": [
            "Synchronized game objects to ensure they cannot be accessed by multiple threads at once",
            "Batching now checks your position to make sure you haven't moved during the batch (special checks for Firemaking also)",
            "Plugins are now interrupted on trade initiation.",
            "Changed the location where the 'helped_femi' cache key is removed",
            "Removed jsoup as it was causing anti-virus programs to flag the launcher.",
            "Removed an unneeded attribute from Demon Slayer"
          ]
        }
      ]
    },
    {
      "title": "ORSC-5.4.1",
      "date": "2020-06-30",
      "changes": [
        {
          "type": "Features",
          "details": [
            "Re-enable the auction house. Happy trading!",
            "A relative of yours is looking for you in Lumbridge! (Custom content miniquest has been released)"
          ]
        },
        {
          "type": "Bug Fixes",
          "details": [
            "Players can now load multiple clients without the ports becoming broken for the previously loaded client.",
            "Logging out with ::mod or ::dev active will now disable the commands.",
            "Fix a problem with Balrog drops.",
            "Players can no longer trade while in combat.",
            "Fix a client crash when buying or selling X to the store.",
            "Ensure players do not sell or buy 0.",
            "Correct the behaviour of the bank vault gate.",
            "Respawning NPCs now always face north.",
            "The game will now message you if you attempt to pick up an item while in combat.",
            "Preset loading now occurs more rapidly."
          ]
        },
        {
          "type": "Enhancements",
          "details": [
            "The auction house now requires a minimum of 100 skill total to use.",
            "The auction house now has rate limits to avoid complications with exchanging items.",
            "Users may not log out for 5 seconds after performing an auction interaction.",
            "Limit packet spam to a subset of all packets.",
            "Add additional safety checks to various packet handlers.",
            "Adjust some of the NPC definitions to have more consistent organization.",
            "Abstract object definitions to JSON format."
          ]
        }
      ]
    },
    {
      "title": "ORSC-5.4.0",
      "date": "2020-06-24",
      "changes": [
        {
          "type": "Bug Fixes",
          "details": [
            "Authentic cooking now has 1 less tick to cook items.",
            "When talking to NPCs, they will now only face you if they want to talk to you.",
            "Staff commands now work from the online list.",
            "Fix a bug where an exception occurred on the server when discord auction updates were disabled.",
            "Fix issues with shop quantities when buying and selling items.",
            "Ensure staff see proper locations when using ::onlinelist.",
            "Ensure the player is teleported properly when caught by guards during the Tourist Trap quest.",
            "Trufitus no longer says \"Well, just let me see the item and I'll help as much as I can.\" twice.",
            "Find ceril properly after being teleported upstairs during the Hazeel Cult quest.",
            "Ensure when we try to find an npc, if they are busy, we don't crash.",
            "As per authentic play, when fatigued a pickaxe now shows above the players head.",
            "Add two pieces of food to the \"never burn\" list.",
            "Ensure that after Scorpion Catcher the Seer does not say \"Many greetings\" to the player.",
            "Attempt to fix a thread deadlock in combat."
          ]
        },
        {
          "type": "Enhancements",
          "details": [
            "Core delay function now uses tick timing instead of milliseconds to calculate thread pause.",
            "Core mes function no longer calls delay.",
            "Ensure game events are run prior to updating packets to send to the client.",
            "Ground item locations and npc locations are now stored in JSON on the server, instead of within the database.",
            "Use UUID object for the player's unique ID instead of a string.",
            "Ensure when auction house is disabled it is impossible to send auction house commands to the server."
          ]
        },
        {
          "type": "Known Bugs",
          "details": [
            "We are working on a fix for a bug found when using 3rd party tools to interact at inhuman speeds with the auction house."
          ]
        }
      ]
    },
    {
      "title": "ORSC-5.3.0",
      "date": "2020-06-19",
      "changes": [
        {
          "type": "Features",
          "details": [
            "Runecraft has been updated to allow casting curse and enfeeble on existing talismans! When casting curse on a talisman, you will gain 2x experience from crafting runes and the talisman will crumble. When casting enfeeble on a talisman, you will gain 5x experience from crafting runes, and the talisman will explode dealing damage to you and causing your current runecraft stat to deplete.",
            "Fish may now be ground using a pestle and mortar to create a stackable item, Fish Oil! This oil can be used to create Runecraft potions (useful for restoring your lost runecraft from enfeebled talismans), and can also be eaten. Two Runecraft potions are available, by using marrentil or by using avantoe as a base. When fish oil is eaten, it will provide a 50% chance of healing 1 hits. Different fish will provide different quantities of fish oil. Only certain fish may be ground, so be sure to experiment and see which reward fish oil."
          ]
        },
        {
          "type": "Bug Fixes",
          "details": [
            "Goblin Guards now drop big bones instead of normal bones.",
            "Shantay's chest object no longer shows the shantay message on variants outside of Al Kharid.",
            "The ::onlinelist command now properly shows other players for staff.",
            "Auditing of many more quest-specific mob locations, timers, and behaviour.",
            "Regular wool may now be used to entertain cats.",
            "Tasty kebab now has a thinkbubble associated with eating it.",
            "Mobs are now killed only once when attacked with specific circumstances.",
            "Fix some dialog bugs introduced with the recent major audits.",
            "Shops now handle buying and selling more efficiently, and provide the correct coin for items.",
            "Shops now stock a maximum of 65535 of one item stack.",
            "Plugins now only load once per class, ensuring bugs like double-shop are squashed.",
            "Fix a bug that made the JSON library complain on computers running JDK 8."
          ]
        },
        {
          "type": "Enhancements",
          "details": [
            "Fishing spots now deplete more frequently, but also respawn at a quicker rate.",
            "Repeat times are now adjusted to a more reasonable count.",
            "The ::onlinelist command now shows readable locations for staff.",
            "Rune essence is now renamed to Rune stone through the code.",
            "Unidentified herbs now have unique sprites.",
            "Soft clay making is now batched.",
            "Lockpicking doors will now batch until successful.",
            "Skill guides now display additional details for harvested food and Runecraft talismans."
          ]
        }
      ]
    }
  ]
}